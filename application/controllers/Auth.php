<?php
use Twilio\Rest\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        // Load form helper library
        $this->load->helper('form');
        
        // Load form validation library
        $this->load->library('form_validation');
        
        // Load session library
        $this->load->library('session');
        
        // Load database
        $this->load->model('Auth_Model');
        }
	public function index()
	{
        if ($this->session->userdata('logged_in') == TRUE)
            redirect(base_url() . 'index.php/auth/dashboard', 'refresh'); 
		$this->load->view('login');
    }
    public function login()
	{
        if ($this->session->userdata('logged_in') == TRUE)
            redirect(base_url() . 'index.php/auth/dashboard', 'refresh'); 
		$this->load->view('login');
    }
    public function add_hobby()
	{
        if ($this->session->userdata('logged_in') != TRUE)
            redirect(base_url(), 'refresh'); 
		$this->load->view('add_hobby');
    }
    public function register()
	{
		$this->load->view('register');
    }
    public function dashboard()
	{
        if ($this->session->userdata('logged_in') != TRUE)
            redirect(base_url(), 'refresh');
		$this->load->view('dashboard');
    }
    public function register_process()
	{
		
		// set validation rules
		$this->form_validation->set_rules('username', 'username', 'trim|required|alpha_numeric|min_length[4]|is_unique[user.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('phone', 'phone ', 'required|regex_match[/^[0-9]{13}$/]');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('register');
            } else {
			
			// set variables from the form
			$data['username'] = $this->input->post('username');
			$data['email']    = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['phone'] = $this->input->post('phone');
            
            $result = $this->Auth_Model->registration_insert($data);

            
            
			if ($result == TRUE) {
				$data['message_display'] = 'Registration Successfully !';
				// user creation ok
				$this->load->view('login', $data);
				
			} else {
				
				$data['message_display'] = 'username already exist!';
				$this->load->view('register', $data);
				
			}
			
        }
		
    }
    public function add_hobby_process()
	{
		
        if ($this->session->userdata('logged_in') != TRUE)
        redirect(base_url(), 'refresh'); 
			// set variables from the form
			$data['user_id'] = $this->session->userdata('user_id');
			$data['title']    = $this->input->post('title');
            $data['details'] = $this->input->post('details');
            $this->db->insert('hobby', $data);
            $account_sid = '';
            $auth_token = '';
            $twilio_number = '+18442084781';

            $client = new Client($account_sid, $auth_token);
            $client->messages->create(
                $data['phone'],
                array(
                    'from' => $twilio_number,
                    'body' => 'Successfully added a hobby. Hobby Title: '.$data['title'].', Hobby Details: '.$data['details'].'!'
                )
            );
            
//sending email after adding hobby
            $mail = new PHPMailer();                           
            try {
                //Server settings
                $mail->SMTPDebug = 2;                         
                $mail->isSMTP();                   
                $mail->Host = 'smtp.mailgun.org'; 
                $mail->SMTPAuth = true;           
                $mail->Username = 'postmaster@sandbox1206bc688c6c43f0baf09ba4887f2a15.mailgun.org';
                $mail->Password = '08dba681d057ecc3493c6395d1f019c7-b6183ad4-17ef6da1'; 
                $mail->SMTPSecure = 'tls'; 
                $mail->From = 'postmaster@sandbox1206bc688c6c43f0baf09ba4887f2a15.mailgun.org';
                $mail->FromName = 'Delivery Science Hobby App';
                $mail->addAddress($this->session->userdata('email'));                 // Add a recipient

                $mail->WordWrap = 50;                                 // Set word wrap to 50 characters

                $mail->Subject = 'Delivery Science Hobby App';
                $mail->Body    = 'Successfully added a hobby. Hobby Title: '.$data['title'].', Hobby Details: '.$data['details'].'!';
            
                $mail->send();
                echo 'Message has been sent';
            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }



           redirect(base_url() . 'index.php/auth/dashboard', 'refresh');
    }
    public function delete_hobby($hobby_id)
	{
		
        if ($this->session->userdata('logged_in') != TRUE)
        redirect(base_url(), 'refresh'); 
			// set variables from the form
			$this->db->where('hobby_id', $hobby_id);
            $this->db->delete('hobby');
            redirect(base_url() . 'index.php/auth/dashboard', 'refresh');
    }
    
    public function user_login_process() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            if(isset($this->session->userdata['logged_in'])){
                redirect(base_url() . 'index.php/auth/dashboard', 'refresh');
            }
            else{
                redirect(base_url() . 'index.php/auth/login', 'refresh');
            }
        } 
        else {
            $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
            );
            $result = $this->Auth_Model->login($data);
            if ($result == TRUE) {
            
            $username = $this->input->post('username');
            $result = $this->Auth_Model->read_user_information($username);
            if ($result != false) {
            $session_data = array(
            'user_id' => $result[0]->user_id,
            'username' => $result[0]->username,
            'email' => $result[0]->email,
            );
            // Add user data in session
            $this->session->set_userdata('logged_in', $session_data);
            $this->session->set_userdata('user_id', $result[0]->user_id);
            $this->session->set_userdata('username', $result[0]->username);
            $this->session->set_userdata('email', $result[0]->email);
            redirect(base_url() . 'index.php/auth/dashboard', 'refresh');
            }
            } else {
            $data = array(
            'error_message' => 'Invalid Username or Password'
            );
            $this->load->view('login', $data);
            }
        }
    }
    public function logout() {

        // Removing session data
        $sess_array = array(
        'username' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        $data['message_display'] = 'Successfully Logout';
        $this->load->view('login', $data);
        }
}
