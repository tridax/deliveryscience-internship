

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard</title>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
<script src="<?php echo base_url() ?>assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
    .message{
        position: absolute;
        font-weight: bold;
        font-size: 28px;
        color: #6495ED;
        left: 262px;
        width: 500px;
        text-align: center;
    }
    .error_msg{
        color:red;
        font-size: 16px;
    }
</style>
</head>
<body><br><br>
<div class="container">
<a href="<?php echo base_url()?>index.php/auth/logout" class="btn btn-primary">Logout</a>
<br><br>
<a href="<?php echo base_url()?>index.php/auth/add_hobby" class="btn btn-primary">Add Hobby</a>
<br><br><br>
<div class="row">
<?php $this->db->where('user_id', $this->session->userdata('user_id'));
                      $this->db->from('hobby');
                      $results= $this->db->count_all_results();?>
                      <?php if($results > 0){ ?>
<?php
                $hobbys = $this->db->get_where('hobby', array('user_id' => $this->session->userdata('user_id')))->result_array();
                
                foreach ($hobbys as $row) { ?>
                
  <div style="padding-bottom : 30px" class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title"><?php echo $row['title']?></h5>
        <p class="card-text"><?php echo $row['details']?></p>
        <a href="<?php echo base_url()?>index.php/auth/delete_hobby/<?php echo $row['hobby_id']?>" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
 
                <?php } ?>
                <?php } ?>
                <?php if($results == 0){ ?>
                    No hobbies available
                <?php } ?>
</div>
</div>
</body>
</html>                                		                            

