<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add Hobby</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
    .message{
        position: absolute;
        font-weight: bold;
        font-size: 28px;
        color: #6495ED;
        left: 262px;
        width: 500px;
        text-align: center;
    }
    .error_msg{
        color:red;
        font-size: 16px;
    }
</style>
</head>
<body>

<div class="login-form">


<?php echo form_open(base_url() . 'index.php/auth/add_hobby_process' , array('class' => 'form-horizontal form-bordered form-validation','id' => 'form', 'enctype' => 'multipart/form-data'));?>
        <h2 class="text-center">Add Hobby</h2>       
        <div class="form-group">
            <input type="text" class="form-control" name="title" placeholder="Title" required="required">
        </div>
        <div class="form-group">
		<textarea class="form-control" id="" rows="3" name="details" placeholder="Details" required="required"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Add</button>
        </div>       
    </form>
    <p class="text-center"><a href="<?php echo base_url();?>index.php/auth/dashboard">Go back to Dashboard</a></p>
</div>
</body>
</html>                                		                            